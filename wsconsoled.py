#!/usr/bin/env python

from gevent import pywsgi, sleep, Timeout
from gevent.select import select
from geventwebsocket.handler import WebSocketHandler
import pyxen
import fcntl, os
import hashlib
import json

def parse_path(path):
    try:
        root, api, vm, key = path.split('/')
        return api, vm, key
    except:
        print 'using deprecated v1 websocket api (console only)'
        root, vm, key = path.split('/')
        return 'console', vm, key

def validate(vm, key):
    secret = open('/etc/edia.passphrase').readlines()[0].strip()
    hash_payload = '{0}:{1}'.format(vm, secret)
    hash = hashlib.sha512(hash_payload).hexdigest()
    return (hash == key)

def write_evtchn(domain, message):
    evtchn_wr = open(domain.get_console_pty(), 'w')
    evtchn_wr.write(message)
    evtchn_wr.close()

def console_api(environ, start_response, vm, key, ws):
    if not ws:
        return

    input = environ["wsgi.input"]
    sock = input.socket if input.socket != None else input.rfile._sock
    session = pyxen.Session()
    domain = session.find_domain(vm)
    evtchn_rd = open(domain.get_console_pty(), 'r')
    fcntl.fcntl(evtchn_rd, fcntl.F_SETFL, os.O_NONBLOCK)

    ws.send('[ Serial console opened to {},\r\n  you may need to press RET for prompt. ]\r\n\r\n'.format(domain.name))

    while True:
        (rlist, wlist, xlist) = select([sock, evtchn_rd], [], [])
        if evtchn_rd in rlist:
            buf = []
            while (select([evtchn_rd], [], [], 0)[0] != []):
               data = evtchn_rd.read(1)
               if not data:
                   ws.send(''.join(buf))
               buf.append(data)
               if len(buf) > 1024:
                   ws.send(''.join(buf))
                   buf = []
            ws.send(''.join(buf))
        if sock in rlist:
            message = ws.receive()
            if message:
                write_evtchn(domain, message)
            else:
                return

def stats_api(environ, start_response, vm, key, ws):
    session = pyxen.Session()
    domain = session.find_domain(vm)

    if not ws:
        return

    while True:
        ws.send(json.dumps(domain.stats()) + '\r\n')
        with Timeout(0.5, False) as timeout:
            value = ws.receive()
            if not value:
                return

def ping_api(environ, start_response, vm, key, ws):
    session = pyxen.Session()
    domain = session.find_domain(vm)

    start_response('200 OK', [("Content-Type", "application/json")])

    state = {
        "running": domain is not None,
    }

    payload = json.dumps(state)
    qstr = environ.get("QUERY_STRING", None)
    if qstr and qstr.startswith('callback='):
        qdata = qstr.partition('&')[0]
        token = qdata[9:]
        return token + '(' + payload + ');'
    return payload

apitab = {
    'console': console_api,
    'stats': stats_api,
    'ping': ping_api,
}

def websocket_app(environ, start_response):
    ws = environ.get("wsgi.websocket", None)
    api, vm, key = parse_path(environ["PATH_INFO"])

    if not validate(vm, key):
        ws.send('unauthorized\n')
        return

    return apitab[api](environ, start_response, vm, key, ws)

server = pywsgi.WSGIServer(("", 9393), websocket_app, handler_class=WebSocketHandler,
			   keyfile='wsconsoled.key', certfile='wsconsoled.crt')
server.serve_forever()
