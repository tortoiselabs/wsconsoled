#!/usr/bin/env python

import hashlib, sys

secret = open('/etc/edia.passphrase').readlines()[0].strip()
hash_payload = '{0}:{1}'.format(sys.argv[1], secret)
print hashlib.sha512(hash_payload).hexdigest()
